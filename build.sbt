name := """Url_shortener"""
organization := "viv"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.13.10"

libraryDependencies ++= Seq(guice, javaJdbc, javaJpa, ehcache,
  "org.hibernate" % "hibernate-entitymanager" % "5.2.11.Final",
  "org.hibernate" % "hibernate-ehcache" % "5.2.11.Final",
  "org.hibernate" % "hibernate-spatial" % "5.2.11.Final",
  "org.hibernate.javax.persistence" % "hibernate-jpa-2.1-api" % "1.0.0.Final",
  "org.xerial" % "sqlite-jdbc" % "3.40.0.0",
  "com.zsoltfabok" % "sqlite-dialect" % "1.0",
  "org.mockito" % "mockito-core" % "4.11.0" % Test
)
