package entity;

import javax.persistence.*;

/**
 * @author vahid
 */
@Entity
@Table(name = "t_url")
@NamedQueries({
        @NamedQuery(
                name = Url.QUERY_FIND_BY_BASE_URL,
                query = "select u from Url u where u.baseUrl = :baseUrl"),
        @NamedQuery(
                name = Url.QUERY_FIND_BY_SHORT_URL,
                query = "select u from Url u where u.shortUrl = :shortUrl"),
        @NamedQuery(
                name = Url.QUERY_UPDATE_SHORT_URL,
                query = "update Url set shortUrl = :shortUrl where id = :id"),
        @NamedQuery(
                name = Url.QUERY_INCREASE_HIT_RATE,
                query = "update Url set hit = hit + 1 where id = :id")
})
public class Url {

    private static final long serialVersionUID = -762202760036583131L;
    public static final String QUERY_FIND_BY_BASE_URL = "url.findByBaseUrl";
    public static final String QUERY_FIND_BY_SHORT_URL = "url.findByShortUrl";
    public static final String QUERY_UPDATE_SHORT_URL = "url.updateShortUrl";
    public static final String QUERY_INCREASE_HIT_RATE = "url.increaseHitRate";


    private long id;
    private String baseUrl;
    private String shortUrl;
    private int hit;


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "c_id")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Column(name = "c_base_url")
    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    @Column(name = "c_short_url")
    public String getShortUrl() {
        return shortUrl;
    }

    public void setShortUrl(String shortUrl) {
        this.shortUrl = shortUrl;
    }

    @Column(name = "c_hit")
    public int getHit() {
        return hit;
    }

    public void setHit(int hit) {
        this.hit = hit;
    }
}
