package controllers;

import com.fasterxml.jackson.databind.node.ObjectNode;
import javassist.NotFoundException;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Results;
import services.UrlService;

import javax.inject.Inject;

public class ReportController {

    private final UrlService urlService;

    @Inject
    public ReportController(UrlService urlService) {
        this.urlService = urlService;
    }

    @BodyParser.Of(BodyParser.Json.class)
    public Result hitRate(Http.Request request) {

        String url = request.body().asJson().get("url").textValue();
        ObjectNode result = Json.newObject();
        try {
            int hitRate = urlService.getHitRate(url);
            result.put("hitNumber", hitRate);
            return Results.ok(result);
        } catch (NotFoundException e) {
            result.put("url", url);
            return Results.notFound(result);
        }
    }
}
