package controllers;

import com.fasterxml.jackson.databind.node.ObjectNode;
import javassist.NotFoundException;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Results;
import services.UrlService;

import javax.inject.Inject;

public class ShortenerController {
    private final UrlService urlService;

    @Inject
    public ShortenerController(UrlService urlService) {
        this.urlService = urlService;
    }

    @BodyParser.Of(BodyParser.Json.class)
    public Result generateShortUrl(Http.Request request) {

        String url = request.body().asJson().get("url").textValue();
        ObjectNode result = Json.newObject();
        String shortUrl = urlService.generateShortUrl(url);
        result.put("shortUrl", shortUrl);
        return Results.ok(result);
    }

    @BodyParser.Of(BodyParser.Json.class)
    public Result getBaseUrl(Http.Request request) {

        String url = request.body().asJson().get("url").textValue();
        ObjectNode result = Json.newObject();
        try {
            String baseUrl = urlService.getBaseUrl(url);
            result.put("baseUrl", baseUrl);
            return Results.ok(result);
        } catch (NotFoundException e) {
            result.put("shortUrl", url);
            return Results.notFound(result);
        }
    }
}
