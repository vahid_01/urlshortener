package repositories;

import entity.Url;
import play.db.jpa.JPAApi;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.persistence.Query;
import java.util.List;
import java.util.Optional;

/**
 * @author vahid
 */
@Singleton
public class UrlRepository {

    private final JPAApi jpaApi;

    @Inject
    public UrlRepository(JPAApi jpaApi) {
        this.jpaApi = jpaApi;
    }

    public Optional<Url> findByBaseUrl(String baseUrl) {

        Query query = jpaApi.em("default").createNamedQuery(Url.QUERY_FIND_BY_BASE_URL, Url.class);
        query.setParameter("baseUrl", baseUrl);
        List<Url> resultList = query.getResultList();
        return resultList.isEmpty() ? Optional.empty() : Optional.of(resultList.get(0));
    }

    public Optional<Url> findByShortUrl(String shortUrl) {
        Query query = jpaApi.em("default").createNamedQuery(Url.QUERY_FIND_BY_SHORT_URL, Url.class);
        query.setParameter("shortUrl", shortUrl);
        List<Url> resultList = query.getResultList();
        return resultList.isEmpty() ? Optional.empty() : Optional.of(resultList.get(0));
    }

    public int updateShortUrl(Long id, String shortUrl) {
        return jpaApi.withTransaction(entityManager -> {
            Query query = entityManager.createNamedQuery(Url.QUERY_UPDATE_SHORT_URL);
            query.setParameter("id", id);
            query.setParameter("shortUrl", shortUrl);
            return query.executeUpdate();
        });
    }

    public int increaseHitRate(Long id) {
        return jpaApi.withTransaction(entityManager -> {
            Query query = entityManager.createNamedQuery(Url.QUERY_INCREASE_HIT_RATE);
            query.setParameter("id", id);
            return query.executeUpdate();
        });
    }

    public void persist(String baseUrl) {
        jpaApi.withTransaction(entityManager -> {
            Url url = new Url();
            url.setBaseUrl(baseUrl);
            url.setHit(0);
            entityManager.persist(url);
            entityManager.flush();
        });
    }


}
