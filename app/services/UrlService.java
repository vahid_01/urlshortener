package services;

import entity.Url;
import javassist.NotFoundException;
import play.api.cache.SyncCacheApi;
import repositories.UrlRepository;
import scala.Option;
import scala.concurrent.duration.Duration;
import scala.reflect.ClassTag;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.Arrays;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

/**
 * @author vahid
 */
@Singleton
public class UrlService {

    private final UrlRepository urlRepository;
    private final SyncCacheApi cache;
    private final int CACHE_DURATION_SECOND = 3600;
    private final String DOMAIN_NAME = "https://viv.ty/";
    final char[] ALPHA_SET = "123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
    final String PREFIX_CACHE_KEY_HIT_RATE = "urlService_hitRate_";
    final String PREFIX_CACHE_KEY_GENERATE = "urlService_generate_";
    final String PREFIX_CACHE_KEY_RETRIEVE = "urlService_retrieve_";


    @Inject
    public UrlService(UrlRepository urlRepository, SyncCacheApi cache) {
        this.urlRepository = urlRepository;
        this.cache = cache;
    }

    public int getHitRate(String shortUrl) throws NotFoundException {

        Option<Integer> urlResult = cache.get(PREFIX_CACHE_KEY_HIT_RATE + shortUrl, ClassTag.apply(Integer.class));
        if (urlResult.isEmpty()) {
            Optional<Url> hitRateFromDB = getHitRateFromDB(shortUrl);
            if (hitRateFromDB.isPresent()) {
                Duration finiteDuration = Duration.create(CACHE_DURATION_SECOND, TimeUnit.SECONDS);
                cache.set(PREFIX_CACHE_KEY_HIT_RATE + shortUrl, hitRateFromDB.get().getHit(), finiteDuration);
                return hitRateFromDB.get().getHit();
            }
            throw new NotFoundException("The url not exist");
        }

        return urlResult.get();
    }

    public String generateShortUrl(String baseUrl) {

        Option<String> urlResult = cache.get(PREFIX_CACHE_KEY_GENERATE + baseUrl, ClassTag.apply(String.class));
        if (urlResult.isEmpty()) {
            Optional<Url> shortUrlFromDB = getShortUrlFromDB(baseUrl);
            if (shortUrlFromDB.isPresent()) {
                Duration finiteDuration = Duration.create(CACHE_DURATION_SECOND, TimeUnit.SECONDS);
                cache.set(PREFIX_CACHE_KEY_GENERATE + baseUrl, shortUrlFromDB.get().getShortUrl(), finiteDuration);
                return shortUrlFromDB.get().getShortUrl();
            }
            urlRepository.persist(baseUrl);
            Optional<Url> shortUrlAfterPersist = getShortUrlFromDB(baseUrl);
            if (shortUrlAfterPersist.isPresent()) {
                String newShortUrl = DOMAIN_NAME + encodeUrl(shortUrlAfterPersist.get().getId(), ALPHA_SET.length);
                int numberRowUpdate = urlRepository.updateShortUrl(shortUrlAfterPersist.get().getId(), newShortUrl);
                if (numberRowUpdate == 1) {
                    Duration finiteDuration = Duration.create(CACHE_DURATION_SECOND, TimeUnit.DAYS);
                    cache.set(PREFIX_CACHE_KEY_GENERATE + baseUrl, newShortUrl, finiteDuration);
                    return newShortUrl;
                }
            }
            return null;
        }
        return urlResult.get();
    }

    public String getBaseUrl(String shortUrl) throws NotFoundException {

        Option<Url> urlResult = cache.get(PREFIX_CACHE_KEY_RETRIEVE + shortUrl, ClassTag.apply(Url.class));
        if (urlResult.isEmpty()) {
            Optional<Url> baseUrlOptional = urlRepository.findByShortUrl(shortUrl);
            if (!baseUrlOptional.isPresent()) {
                throw new NotFoundException("The url not exist");
            }
            int numberRowUpdate = urlRepository.increaseHitRate(baseUrlOptional.get().getId());
            if (numberRowUpdate == 1) {
                Duration finiteDuration = Duration.create(CACHE_DURATION_SECOND, TimeUnit.DAYS);
                cache.set(PREFIX_CACHE_KEY_RETRIEVE + shortUrl, baseUrlOptional.get(), finiteDuration);
                cache.remove(PREFIX_CACHE_KEY_HIT_RATE + shortUrl);
            }
            return baseUrlOptional.get().getBaseUrl();
        }
        int numberRowUpdate = urlRepository.increaseHitRate(urlResult.get().getId());
        if (numberRowUpdate == 1) {
            Duration finiteDuration = Duration.create(CACHE_DURATION_SECOND, TimeUnit.DAYS);
            cache.set(PREFIX_CACHE_KEY_RETRIEVE + shortUrl, urlResult.get(), finiteDuration);
            cache.remove(PREFIX_CACHE_KEY_HIT_RATE + shortUrl);
        }
        return urlResult.get().getBaseUrl();
    }

    private Optional<Url> getHitRateFromDB(String shortUrl) {
        return urlRepository.findByShortUrl(shortUrl);
    }

    private Optional<Url> getShortUrlFromDB(String baseUrl) {
        return urlRepository.findByBaseUrl(baseUrl);
    }

    private String getBaseUrlFromDB(String shortUrl) {
        Optional<Url> baseUrlOptional = urlRepository.findByShortUrl(shortUrl);
        return baseUrlOptional.map(Url::getBaseUrl).orElse(null);
    }

    /**
     * I got source from this link. https://itnext.io/url-shortening-explained-2e524b1f8d4f
     *
     * @param id   the id entity from db
     * @param base the length of alpha set
     * @return short path
     */

    private String encodeUrl(long id, int base) {
        StringBuilder sb = new StringBuilder();
        while (id > 0) {

            // find out what the right most character is in base 10
            int toBaseTen = (int) (id % base);

            // put the charcter in the right-most position of the result
            sb.append(ALPHA_SET[toBaseTen]);

            // divide the number by the base to get ready to evaluate the next position
            id = (int) Math.floor(id / base);
        }
        return sb.toString();
    }

    /**
     * I got source from this link. https://itnext.io/url-shortening-explained-2e524b1f8d4f
     *
     * @param shortUrl the short url path
     * @param base     the length of alpha set
     * @return the base url
     */
    private int decodeUrl(String shortUrl, int base) {
        StringBuilder sb = new StringBuilder();
        while (shortUrl.length() > 0) {

            // see what each character equals in base 10
            int alphaIndex = Arrays.toString(ALPHA_SET).indexOf(shortUrl.charAt(0));

            // find the number placment to act as base
            int power = shortUrl.length() - 1;

            // increment the result
            sb.append(alphaIndex * (Math.pow(base, power)));

            // remove the left-most character in the url
            shortUrl = shortUrl.substring(1);
        }
        return Integer.parseInt(sb.toString());
    }
}
